mod luthor;
use std::io::copy;
use std::iter::Peekable;
use luthor::{Lexer, Token} ;

fn main() {

    let input = "+={}super_duper pooper ()=";

    let lex = Lexer::new(input);

    for tk in lex {

        println!("{:?}", tk);
    }

}
