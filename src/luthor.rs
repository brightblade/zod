//lets write a lexer for zod
//the first version is just a clone of 'Monkey' from the writing an interpreter in Go.
//I'm doing it in rust and pretending that go is pseudo code.
use std::error::Error;
use std::iter::Peekable;

struct TokenInfo {
    Type: String,
    Data: String,
    LineNo: Option<u32>,
    Column: Option<u32>,
}

#[derive(Debug, PartialEq)]
pub enum Token {
    //Identifiers + literals
    Identifier(String),
    Int(i32),
    //keywords
    Let,
    Function,
    True,
    False,
    If,
    Else,
    Return,
    //Operators
    Assign,
    Plus,
    Minus,
    Bang,
    Asterisk,
    Slash,
    Equal,
    NotEqual,
    LessThan,
    GreaterThan,
    //delimiters
    Comma,
    SemiColon,
    LParen,
    RParen,
    LBrace,
    RBrace,

    Illegal,
    EOF,
}

#[derive(Debug, PartialEq)]
pub struct Lexer {
   pub input: String,  //the source code
   pub  position: usize, //current position in input (points to the current char)
   pub read_position: usize, //current reading position in input (after current char) for peeking. shh
   pub ch: Option<u8>, //current char under examination, or None
}

impl Iterator for Lexer {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        if self.position == 0 {
            self.read_char(); //kick things off
        }

        self.skip_whitespace();
        let tok = match self.ch  {
            Some(b'=') => Token::Assign,
            Some(b'+') => Token::Plus,
            Some(b'-') => Token::Minus,
            Some(b'*') => Token::Asterisk,
            Some(b'/') => Token::Slash,
            Some(b'{') => Token::LBrace,
            Some(b'}') => Token::RBrace,
            Some(b'(') => Token::LParen,
            Some(b')') => Token::RParen,
            Some(b',') => Token::Comma,
            Some(b';') => Token::SemiColon,
            Some(b'<') => Token::LessThan,
            Some(b'>') => Token::GreaterThan,
            Some(b'!') => {

                Token::Bang
            },
            Some(b'>') => Token::GreaterThan,

            Some(c) if (c as char).is_alphabetic() => {
                let ident = self.read_identifier();
                Token::Identifier(ident)
            }
            _ => return None
        };

        self.read_char();
        Some(tok)
    }
}

impl Lexer {
    pub fn new(input: &str) -> Self {
        Lexer {
            input: String::from(input),
            position: 0,
            read_position: 0,
            ch: None,
        }
    }

    fn read_char(&mut self) {
        if self.read_position >= self.input.len() {
            self.ch = None;
        } else {
            self.ch = Some(self.input.as_bytes()[self.read_position]);
            self.position = self.read_position;
            self.read_position = self.read_position + 1;
        }
    }

    fn read_identifier(&mut self) -> String {
        let mut ident = String::new(); //String::from_utf8(vec![self.ch.unwrap()]).unwrap();
        /*while let Some(c) = self.ch {
            if (c as char).is_alphabetic() {
                ident.push(c as char);
            }

            self.read_char();
        }*/

        while self.is_letter() {
            if let Some(c) = self.ch  {
                ident.push(c as char);
                self.read_char();
            }
        }

        ident
    }

    fn is_letter(&mut self) -> bool {
        if self.ch.is_none() {
            return false;
        }
        match self.ch.unwrap() as char {
            '_' | 'a'...'z' | 'A'...'Z' => true,
            _ => false
        }
       // (self.ch.expect("A byte") as char).is_alphabetic()
    }

    fn skip_whitespace(&mut self)  {

        loop {

            if self.ch.is_none() { break; }
            match self.ch.unwrap() as char {
                ' ' | '\t' | '\n' | '\r' => {
                    self.read_char();
                },
                _ => break,
            }
        }
    }
}

fn parse_number<I>(tokens: &mut Peekable<I>) -> String
where
    I: Iterator<Item = char>,
{
    //let mut n = 0;
    let mut tok = String::new();
    loop {
        match tokens.peek() {
            Some(r) if r.is_digit(10) => {
                tok.push_str(&r.to_string());
                //n = n + r;
                //n = n * 10 + r.to_digit(10).unwrap();
                //println!("peeking: {}", n);
            }
            _ => return tok,
        }

        tokens.next();
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn next_token() {
        assert_eq!(1, 1);
    }

}
